/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projectfinalcode;

/**
 *
 * @author informatics
 */
public class Customer {
    int Id;
    String name;
    String surname;
    String tel;

    public Customer(int i, String a, String b, Customer c) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public Customer(int Id, String name, String surname, String tel) {
        this.Id = Id;
        this.name = name;
        this.surname = surname;
        this.tel = tel;
    }
    public Customer(){
        this.Id=-1;
    }

    @Override
    public String toString() {
        return "Customer{" + "Id=" + Id + ", name=" + name + ", surname=" + surname + ", tel=" + tel + '}';
    }
    
}
