/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projectfinalcode;

import projectfinalcode.database;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import static projectfinalcode.database.conn;

/**
 *
 * @author informatics
 */
public class PetsDAO {
    
    public static boolean insert(Pets pet){
       Connection conn = database.Connectdatabase();
        try {
            Statement stm = conn.createStatement();
           String sql = "INSERT INTO Pets (\n" +
"                     name,\n" +
"                     age,\n" +
"                     weight,\n" +
"                     specie,\n" +
"                     type,\n" +
"                     detail,\n" +
"                     Id_custome\n" +
"                 )\n" +
"                 VALUES (\n" +
"                     '%s',\n" +
"                     '%d',\n" +
"                     '%f',\n" +
"                     '%s',\n" +
"                     '%s',\n" +
"                     '%s',\n" +
"                     '%d'\n" +
"                 );";
        System.out.println(String.format(sql,pet.getName(),pet.getAge(),pet.getWeight(),pet.getSpecie(),pet.getType(),pet.getDetail(),pet.getId_customer()));
        stm.execute(String.format(sql,pet.getName(),pet.getAge(),pet.getWeight(),pet.getSpecie(),pet.getType(),pet.getDetail(),pet.getId_customer()));
        database.Closedatabase();
        return true;
        } catch (SQLException ex) {
            Logger.getLogger(CustomersDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        return true;
    }
    
//    public static boolean update(Pets pet){
//        String sql = "UPDATE Customers\n" +
//"   SET name = '%s',\n" +
//"       surname = '%s',\n" +
//"       tel = '%s'\n" +
//" WHERE Id = %d ";
//        Connection conn = database.Connectdatabase();
//        Statement stm ;
//        try {
//            stm = conn.createStatement();
//            boolean ret = stm.execute(String.format(sql, customer.getName(),customer.getSurname(),customer.getTel(),customer.getId()));
//            database.Closedatabase();
//            return ret;
//        } catch (SQLException ex) {
//            Logger.getLogger(CustomersDAO.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        database.Closedatabase();
//        return true;
//    }
    
//    public static boolean delete(Customer customer){
//        String sql = "DELETE FROM Customers\n" +
//"      WHERE Id = %d";
//        Connection conn = database.Connectdatabase();
//
//        try {
//            Statement stm = conn.createStatement();
//            boolean ret = stm.execute(String.format(sql,customer.getId()));
//        database.Closedatabase();
//        return ret;
//        } catch (SQLException ex) {
//            Logger.getLogger(CustomersDAO.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        database.Closedatabase();    
//        return true;
//    }
    
    public static ArrayList<Pets> getPets(){
        ArrayList<Pets> list = new ArrayList();
        database.Connectdatabase();
        try {
            String sql = "SELECT Id,\n" +
"       name,\n" +
"       age,\n" +
"       weight,\n" +
"       specie,\n" +
"       type,\n" +
"       detail,\n" +
"       Id_custome\n" +
"  FROM Pets";
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(sql);
            while(rs.next()){
                Pets pet = rsToObj(rs);
                list.add(pet);
            }
            database.Closedatabase();
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(database.class.getName()).log(Level.SEVERE, null, ex);
        }
        database.Closedatabase();
        return null;
    }
    
    private static Pets rsToObj(ResultSet rs) throws SQLException {
        Pets pet;
        pet = new Pets();
        pet.setId(rs.getInt("id"));
        pet.setName(rs.getString("name"));
        pet.setAge(rs.getInt("age"));
        pet.setWeight(rs.getDouble("weight"));
        pet.setSpecie(rs.getString("specie"));
        pet.setType(rs.getString("type"));
        pet.setDetail(rs.getString("detail"));
        pet.setId_customer(rs.getInt("Id_custome"));
        return pet;
    }
    
    public static Pets getPet(int petId){
        String sql = "SELECT * FROM Pets WHERE Id= %d ";
        Connection conn = database.Connectdatabase();
        try {
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(String.format(sql,petId));
            if(rs.next()){
                Pets pet = rsToObj(rs);
                database.Closedatabase(); 
                return pet;
            }
        } catch (SQLException ex) {
            Logger.getLogger(CustomersDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        database.Closedatabase(); 
        return null;
    }
    
}
