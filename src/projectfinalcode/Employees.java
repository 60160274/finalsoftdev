/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projectfinalcode;

/**
 *
 * @author informatics
 */
public class Employees {
    int Id;
    String username;
    String password;
    String name;
    String surname;

    public Employees() {
    }

  

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public String toString() {
        return "Employees{" + "Id=" + Id + ", username=" + username + ", password=" + password + ", name=" + name + ", surname=" + surname + '}';
    }

    public Employees(int Id, String username, String password, String name, String surname) {
        this.Id = Id;
        this.username = username;
        this.password = password;
        this.name = name;
        this.surname = surname;
    }
    
}
