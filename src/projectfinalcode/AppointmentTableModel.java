/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projectfinalcode;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author MET
 */
public class AppointmentTableModel extends AbstractTableModel{
String[] colunname = {"Recive date","Customer"};
    ArrayList<Appointments> list = AppointmentsDAO.getAppointments();
    ArrayList<Customer> listc = CustomersDAO.getCustomers();

    public AppointmentTableModel(){
    }
    @Override
    public String getColumnName(int i) {
        return colunname[i];
    }
    
    @Override
    public int getRowCount() {

        return list.size();
    }

    @Override
    public int getColumnCount() {
        return colunname.length;
    }

    @Override
    public Object getValueAt(int i, int i1) {
       Appointments app = list.get(i);
       Customer cus = listc.get(i);
       
       if(app==null){ 
            return"";
       }
        switch(i1){
            case 0: return app.getRecive_date();
            case 1: if(app.getId_customer()== cus.getId()){
           return cus.getName()+" "+cus.getSurname();
       }

        }     
        return "";
}
}
