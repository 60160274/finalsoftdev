/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projectfinalcode;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author MET
 */
public class UserTableModel extends AbstractTableModel{
String[] colunname = {"ID","Name"};
ArrayList<Customer> list = CustomersDAO.getCustomers();
    public UserTableModel(){
    
    }
    @Override
    public String getColumnName(int i) {
        return colunname[i];
    }

    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return colunname.length;
    }

    @Override
    public Object getValueAt(int i, int i1) {
        Customer cus = list.get(i);
        if(cus==null) return"";
        switch(i1){
            case 0: return cus.getId();
            case 1: return cus.getName()+" "+cus.getSurname();
        }
        return "";
    }
    
}
