/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projectfinalcode;

import java.sql.Connection;
import projectfinalcode.database;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import static projectfinalcode.database.conn;

/**
 *
 * @author informatics
 */
public class EmployeesDAO {
    
        public static boolean insert(Employees em){
       Connection conn = database.Connectdatabase();
        try {
            Statement stm = conn.createStatement();
           String sql = "INSERT INTO Employees (\n" +
"                          username,\n" +
"                          password,\n" +
"                          name,\n" +
"                          surname\n" +
"                      )\n" +
"                      VALUES (\n" +
"                          '%s',\n" +
"                          '%s',\n" +
"                          '%s',\n" +
"                          '%s'\n" +
"                      );";
        System.out.println(String.format(sql,em.getUsername(),em.getPassword(),em.getName(),em.getSurname()));
        stm.execute(String.format(sql,em.getUsername(),em.getPassword(),em.getName(),em.getSurname()));
        database.Closedatabase();
        return true;
        } catch (SQLException ex) {
            Logger.getLogger(ProductsDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        return true;
    }
    
        public static ArrayList<Employees> getEmployees(){
        ArrayList<Employees> list = new ArrayList();
        database.Connectdatabase();
        try {
            String sql = "SELECT Id,\n" +
"       username,\n" +
"       password,\n" +
"       name,\n" +
"       surname\n" +
"  FROM Employees";
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(sql);
            while(rs.next()){
                Employees emp = rsToObj(rs);
                list.add(emp);
            }
            database.Closedatabase();
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(database.class.getName()).log(Level.SEVERE, null, ex);
        }
        database.Closedatabase();
        return null;
    }
    
    private static Employees rsToObj(ResultSet rs) throws SQLException {
        Employees emp;
        emp = new Employees();
        emp.setId(rs.getInt("id"));
        emp.setUsername(rs.getString("username"));
        emp.setPassword("password");
        emp.setName(rs.getString("name"));
        emp.setSurname(rs.getString("surname"));
        return emp;
    }
}
