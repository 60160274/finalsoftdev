/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projectfinalcode;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import static projectfinalcode.database.conn;

/**
 *
 * @author Dell
 */
public class AppointmentsDAO {

    public static ArrayList<Appointments> get;

    public static boolean insert(Appointments app) {
        Connection conn = database.Connectdatabase();
        try {
            Statement stm = conn.createStatement();
            String Sql = "INSERT INTO Appointments (\n"
                    + "                             arrive_date,\n"
                    + "                             recive_date,\n"
                    + "                             Id_employee,\n"
                    + "                             Id_customer,\n"
                    + "                             Id_room,\n"
                    + "                             Id_pets\n"
                    + "                         )\n"
                    + "                         VALUES (\n"
                    + "                             '%s',\n"
                    + "                             '%s',\n"
                    + "                             '%d',\n"
                    + "                             '%d',\n"
                    + "                             '%d',\n"
                    + "                             '%d'\n"
                    + "                         );";
            System.out.println(String.format(Sql, app.getArrive_date(), app.getRecive_date(), app.getId_employee(), app.getId_customer(), app.getId_room(), app.getId_pets()));
            stm.execute(String.format(Sql, app.getArrive_date(), app.getRecive_date(), app.getId_employee(), app.getId_customer(), app.getId_room(), app.getId_pets()));
            database.Closedatabase();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(AppointmentsDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        database.Closedatabase();
        return true;
    }

    public static ArrayList<Appointments> getAppointments() {
        ArrayList<Appointments> list = new ArrayList();
        database.Connectdatabase();
        try {
            String sql = "SELECT Id,\n"
                    + "       arrive_date,\n"
                    + "       recive_date,\n"
                    + "       Id_employee,\n"
                    + "       Id_customer,\n"
                    + "       Id_room,\n"
                    + "       Id_pets\n"
                    + "  FROM Appointments;";
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                Appointments app = rsToObj(rs);
                list.add(app);
            }
            database.Closedatabase();
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(database.class.getName()).log(Level.SEVERE, null, ex);
        }
        database.Closedatabase();
        return null;
    }

    private static Appointments rsToObj(ResultSet rs) throws SQLException {
        Appointments app;
        app = new Appointments();
        app.setId(rs.getInt("id"));
        app.setArrive_date(rs.getString("arrive_date"));
        app.setRecive_date(rs.getString("recive_date"));
        app.setId_employee(rs.getInt("Id_employee"));
        app.setId_customer(rs.getInt("Id_customer"));
        app.setId_room(rs.getInt("Id_room"));
        app.setId_pets(rs.getInt("Id_pets"));
        return app;
    }

    public static Appointments getAppointment(int appId) {
        String sql = "SELECT * FROM Appointments WHERE Id= %d ";
        Connection conn = database.Connectdatabase();
        try {
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(String.format(sql, appId));
            if (rs.next()) {
                Appointments app = rsToObj(rs);
                database.Closedatabase();
                return app;
            }
        } catch (SQLException ex) {
            Logger.getLogger(AppointmentsDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        database.Closedatabase();
        return null;
    }

}