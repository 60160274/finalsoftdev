/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projectfinalcode;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import projectfinal.DepositSearchCustomerPanel;
import static projectfinal.DepositSearchCustomerPanel.selectcus;

/**
 *
 * @author MET
 */
public class PetsTableModel extends AbstractTableModel{
String[] colunname = {"ID","Name"};
    int user = DepositSearchCustomerPanel.selectcus;
    ArrayList<Pets> list = PetsDAO.getPets();
    ArrayList<Pets> pet ;

    
    public PetsTableModel(){
    }
    @Override
    public String getColumnName(int i) {
        return colunname[i];
    }
    
    @Override
    public int getRowCount() {

        return list.size();
    }

    @Override
    public int getColumnCount() {
        return colunname.length;
    }

    @Override
    public Object getValueAt(int i, int i1) {
       Pets pet = list.get(i);
       if(pet==null){ 
            return"";
       }
       if(pet.getId_customer()==user){
        switch(i1){
            case 0: return pet.getId();
            case 1: return pet.getName();
        }     
       }
        return "";
}
}
