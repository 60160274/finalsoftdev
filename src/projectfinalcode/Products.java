/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projectfinalcode;

/**
 *
 * @author informatics
 */
public class Products {
    int Id;
    String name;
    String type;
    int quantity;
    int price;

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Products{" + "Id=" + Id + ", name=" + name + ", type=" + type + ", quantity=" + quantity + ", price=" + price + '}';
    }

    public Products(int Id, String name, String type, int quantity, int price) {
        this.Id = Id;
        this.name = name;
        this.type = type;
        this.quantity = quantity;
        this.price = price;
    }

    public Products() {
        this.Id = -1;
    }
    
}
